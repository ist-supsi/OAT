import oatlib
import requests
import json
from oatlib import sensor, method, oat_utils
from dateutil import parser, relativedelta
import matplotlib.pyplot as plt
from statistics import stdev
from datetime import datetime, tzinfo, timedelta
import dateutil

server = 'https://geoservice.ist.supsi.ch/vedeggio'
service = 'vedeggio'
user = 'alberto.vancheri@supsi.ch'
pwd = 'pt2RkNw'

#event_time = '2016-03-11T11:20:00+0100/2019-09-06T14:00:00+0100'
event_time = '2019-09-05T11:20:00+0100/2019-09-06T14:00:00+0100'

observed_property_name = 'water-height'
observed_property_uom = 'm'

istsos_url = '{}/{}'.format(server, service)
basic_auth = (user, pwd)

SENSOR = sensor.Sensor(
    name='VED_MAN',
    prop='water:height',
    unit='m'
)

SENSOR.ts_from_istsos(
    service=istsos_url,
    observed_property='water:height',
    procedure='VED_MAN',
    basic_auth=(user, pwd),
#     aggregate_function='AVG',
#     aggregate_interval='PT10M',
    event_time=event_time,
    delta=relativedelta.relativedelta(days=15eve    )
)

print(SENSOR)